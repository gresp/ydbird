
Description rapide de la structure du dbird.

Dans cette description les <dbird code> sont des code uniques et arbitraires servant à 
effectuer des références à l'intérieur du fichier. 
Le format des date est : YYYY-MM-DDTHH:MM:SSZ (Ex: 1962-01-01T00:00:00Z)
                         On utilise "presen"t comme date de fin quand une période n'est pas encore fermée 

Les liste ont comme séparateur la virgile (',')
Le fichier utilise le système de référence de YAML:
  - & pour déclarer un objet
  - * pour réferencer un objet précédement déclarer
   
Les chemins référençant les fichier PZ sont relatifs à la racine de la banque de données.


Desciption de la structure du fichier ydbird:
------

version: 2
originating_organization: [ "<Description de l'organisme générant les données>", "<mail de contact de l'organisation>", "<URL de l'organisation>", "<Téléphone de l'organisation>" ]

author:
     - &<dbird code> [ ["<Nom et Prénom de l'auteur>"], [<Liste vide pour utilisation futures>], [<liste des emails>], [<liste des téléphones>] ]

network:
  comment:
     - &<dbird code> [ "<Sujet du commentaire>", "<Commentaire>", "<date de début du commentaire>", "<date de fin du commentaire>", [<Liste des auteurs du commentaire (référence au dbird code présent dans la section author)>], "N" ]

  network:
     - &<dbird code> [ "<FDSN code>", "<Description>", "<Email contact>", "<Date ouverture>", "<Date de fermeture>", "<Alternate code>", "<DOI>",  [<Liste des commentaires associés au réseau (référence au dbird code de la sous section comment de la section network)>] ]

station:
     -
       definition: [ "<IRIS code>", "<Date d'installation>", "<Date de fermeture>", [<Latitude>, <Latitude error min ou null si manquant>,<Latitude error plus or null si manquant>], [<Longitude>, <Longitude error min ou null si manquant>, <Longitude error plus or null si manquant>], [<Altitude>, <Altitude error min ou null si manquant>, <Altitude error plus or null si manquant>], "<toponyme>" ]
       contact: "<Station email contact>"
       owner: "<Owner de la station>"
       site:
         name: "<Nom du site>"
         description: "<Description du site (Administrative code pour RESIF il me semble)>"
         town: "<Ville>" 
         county: "<Département>" 
         region: "<Région>" 
         country: "<Pays>" 
       owner_website: "<URL pour le owner de la station>"

       comment:
         - &<dbird code> [ "<Sujet>", "<Commentaire>", "<Date de début>", "<Date de fin>", [<Liste des auteurs>], <"S" pour des commentaires station et "C" pour des commentaires channel> ]
       location:
         - &<dbird code> [ "<Location code>", [<Latitude>, <Latitude error min ou null si manquant>,<Latitude error plus or null si manquant>], [<Longitude>, <Longitude error min ou null si manquant>, <Longitude error plus or null si manquant>], [<Altitude>, <Altitude error min ou null si manquant>, <Altitude error plus or null si manquant>], <Profondeur (toujours positif)>, "<vault>", "<geology>" ]

       sensor:
         - &<dbird code> [ "<Modèle>", "<Numéro de série>", "<Fichier PZ>", <Azimuth>, <Dip> ]

       ana_filter:
         - &<dbird code> [ "<Modèle>", "<Numéro de série>", "<Fichier PZ>" ]

       digitizer:
         - &<dbird code> [ "Modèle", "<Numéro de série>", "<Fichier PZ>" ]

       decimation:
         - &<dbird code> [ "<Modèle>", "<Numéro de série>", "<Fichier PZ>" ]

       channel:
         - [ <Location dbird code>, <IRIS code>, <Sensor dbird code>, [ <Liste des filtres analogiques>], <Digitizer dbird code>, [ Liste des filtres numériques], "<Channel flags>", "<Format des données>", "<Date d'ouverture>", "<Date de fermeture>", <Fréquence>, <Network dbird code>, [<Liste des commentaires associés au canal>] ]

---




